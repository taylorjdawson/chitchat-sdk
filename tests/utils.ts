import path from 'path'
import { Polly } from '@pollyjs/core'
import NodeHttpAdapter from '@pollyjs/adapter-node-http'
import FSPersister from '@pollyjs/persister-fs'

export const setUpPolly = (recordingName: string): Polly => {
  Polly.register(FSPersister)
  Polly.register(NodeHttpAdapter)

  const polly = new Polly(`${recordingName}`, {
    // _${Date.now()}
    adapters: ['node-http'],
    persister: 'fs',
    persisterOptions: {
      fs: {
        recordingsDir: path.resolve(__dirname, 'recordings'),
      },
    },
    matchRequestsBy: {
      headers: {
        exclude: ['x-txn-time', 'x-query-time', 'x-storage-bytes-read', 'date'],
      },
      body(body) {
        const json = JSON.parse(body)
        delete json.params?.object.data.object.id
        return JSON.stringify(json)
      },
    },
  })
  const { server } = polly
  server.any().on('beforePersist', (req, recording) => {
    recording.request.headers = recording.request.headers.filter(
      ({ name }: { name: string }) => name !== 'authorization',
    )
  })
  return polly
}
