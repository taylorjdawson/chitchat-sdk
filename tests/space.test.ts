import dotenv from 'dotenv'
dotenv.config()
import anyTest, { TestInterface } from 'ava'
import { Polly } from '@pollyjs/core'

import Fauna from '../src/models/fauna'
import { Space } from '../src/models'
import { setUpPolly } from './utils'

const test = anyTest as TestInterface<{ space: Space; polly: Polly }>

/**********************************
 * -------- Tests Setup ---------- *
 **********************************/
test.before((t) => {
  t.context.polly = setUpPolly('space')
})

test.after(async (t) => {
  await t.context.polly.stop()
})

test.beforeEach((t) => {
  t.context.space = new Space(new Fauna(process.env.FAUNA_ADMIN_TOKEN ?? ''))
})

test('Can create and delete space', async (t) => {
  const name = 'TEST_SPACE'
  const createResult = await t.context.space.create(name)
  t.is(createResult.name, name)
  const deleteResult = await t.context.space.delete(name)
  t.is(deleteResult.name, name)
})

test.todo("Can't delete space if not owner")
test.todo("Can't create space if not logged in")
