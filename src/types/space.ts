import { User } from './user'
import { Message } from './message'

export interface Role {
  // The id of the user to which this role pertains
  userId: string

  // The type of role i.e. Owner
  type: string
}

/**
 * Represents a space where users can exchange messages.
 */
export interface Space {
  // the uuid of the space
  id: string
  // The name of the space (unique and case insensitive)
  name: string
  // the list of users within the space
  users: User[]
  // the list of messages sent within the space
  messages: Message[]
  // The list of roles that each user in the space has
  roles: Role[]
  // The time when the 'Space' was created
  //createdOn: Date
}
