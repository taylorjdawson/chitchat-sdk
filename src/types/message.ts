/**
 * Represents a message that users can send/receive
 */
export interface Message {
  // The uuid of the message
  id: string
  // The messages content such as the text content
  content: Content
  // When the message was sent
  timestamp: Date
  // This is the id of the space if it is a group message or the user if it is a direct message
  recipient: string
  // List of read receipts per user
  readReceipts: ReadReceipt
}

// Note: This may have to encode for format in the future
interface Content {
  text: string
}

/**
 * Keeps record of when a message was delivered and to who, as well as,
 * who read the message and when.
 * delivered - the message reached the users device
 * seen - the message is displayed on the users screen
 */
interface ReadReceipt {
  // The id of the user who read/received the message
  userId: string
  // The time when the message was 'seen' by the user (displayed on the users screen)
  seenAt: Date | null
  // The time the message was 'delivered' to the user
  deliveredAt: Date | null
}
