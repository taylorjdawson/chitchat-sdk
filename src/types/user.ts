export interface User {
  // The uuid of the user
  id: string
  // The name of the user (like first/last name)
  name: string
  // The unique case insensitive displayed name of the user
  username: string
  // The url of the users avatar to be displayed
  avatarUrl: string
  // List of ids of spaces that this user is a part of
  spaces: string[]
  // The list of direct message ids
  messages: string[]
}
