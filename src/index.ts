import Fauna from './models/fauna'
import { Space } from './models'

export default class ChitChat {
  // The token used when authenticating requests to the backend.
  token: string

  // The Fauna db client
  fauna: Fauna

  // The Space instance used for managing spaces
  space: Space

  /**
   * Instantiates the ChitChat class.
   * @param token - the token used when authenticating requests to the backend.
   */
  constructor(token: string) {
    this.token = token
    this.fauna = new Fauna(token)
    this.space = new Space(this.fauna)
  }
}
