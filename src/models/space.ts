import Fauna from './fauna'
import { v4 as uuid } from 'uuid'
import { Space as SpaceType } from '../types'
import { validate as isUuid } from 'uuid'

const SPACE_DEFAULTS = { users: [], messages: [], roles: [] }

type SlimSpace = Pick<SpaceType, 'users' | 'messages' | 'roles'>

const INDEX = {
  NAME: 'spaces_by_name',
  ID: 'spaces_by_id',
}

export class Space {
  // The Fauna db client
  fauna: Fauna

  // The name of the fauna collection
  static CollectionName = 'spaces'

  constructor(fauna: Fauna) {
    this.fauna = fauna
  }

  /**
   * Creates a new space.
   * @param name - the name of the space
   * @param options - extra Space data to include at creation
   */
  async create(name: string, options: SlimSpace = SPACE_DEFAULTS): Promise<SpaceType> {
    return this.fauna.create<SpaceType>(Space.CollectionName, { id: uuid(), name, ...options })
  }

  /**
   * Deletes a space.
   * @param key - the key used to reference the space which will be deleted.
   * The key is either the space 'name' or the space 'id'
   */
  delete(key: string): Promise<SpaceType> {
    return this.fauna.delete<SpaceType>(Space.CollectionName, isUuid(key) ? INDEX.ID : INDEX.NAME, key)
  }
}
