import { Client, query as q, values } from 'faunadb'
import Ref = values.Ref

const ERRORS = {
  INVALID_REF: 'invalid ref',
  NOT_UNIQUE: 'instance not unique',
}

class FaunaError extends Error {
  constructor(message: string) {
    super(message)
    this.name = 'FaunaError'
  }
}

class NotUniqueError extends FaunaError {
  constructor(instance: string) {
    super(`${instance} already exists`)
    this.name = 'NotUniqueError'
  }
}

class NonExistentError extends FaunaError {
  constructor(collection: string) {
    super(`collection '${collection}' does not exists`)
    this.name = 'NotUniqueError'
  }
}

/**
 * Generic interface for data with an 'id'
 */
interface Id {
  // The uuid of the data
  id: string
}

interface FaunaResult<T> {
  ref: Ref
  ts: number
  data: T
}

const handleCollectionErrors = <T extends Id>(error: Error, collection: string, data: T) => {
  switch (error.message) {
    // Indicates that the collection does not exist
    case ERRORS.INVALID_REF:
      throw new NonExistentError(collection)
    // Indicates that the document already exists
    case ERRORS.NOT_UNIQUE:
      throw new NotUniqueError(data['id'])
    default:
      throw new FaunaError(error.message)
  }
}

export default class Fauna {
  // Fauna client token
  client: Client

  /**
   * Instantiates Fauna class
   * @param token - Fauna client token
   */
  constructor(token: string) {
    this.client = new Client({
      secret: token,
      domain: 'db.fauna-preview.com',
    })
  }

  /**
   * Issues a create query on a given collection
   * @param collection - the collection in which to create the new document
   * @param data - the data associated with the document
   */
  async create<T extends Id>(collection: string, data: T): Promise<T> {
    // TODO: Can we show which field of the data has the unique constraint in the error message?
    const result = await this.client
      .query<FaunaResult<T>>(q.Create(q.Collection(collection), { data: data }))
      .catch((error) => handleCollectionErrors(error, collection, data))
    return result.data
  }

  /**
   * Deletes a document from a collection.
   * @param collection - the collection from which to delete a document
   * @param index - the index on the collection
   * @param key - the key used to identify the document
   * @return the result of the query
   */
  async delete<T>(collection: string, index: string, key: string): Promise<T> {
    const result = await this.client.query<FaunaResult<T>>(
      q.Delete(q.Select('ref', q.Get(q.Match(q.Index(index), key)))),
    ) // TODO: Handle errors
    return result.data
  }
}
/*

serverClient.query(
  q.Get(
    q.Match(q.Index('posts_by_title'), 'My cat and other marvels')
  )
)
 */
