import { uniqueNamesGenerator, adjectives, colors, names } from 'unique-names-generator'

export const randomName = (): string => {
  return uniqueNamesGenerator({
    dictionaries: [adjectives, colors, names],
    separator: ' ',
    length: 3,
  })
}
